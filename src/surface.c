#include "surface.h"
#include "popup.h"
#include "types.h"
#include "view.h"
#include <stdlib.h>

flyaway_surface_t* new_flyaway_surface_from_view(flyaway_view_t* view) {
    flyaway_surface_t* surface = calloc(1, sizeof(flyaway_surface_t));
    surface->type = FLYAWAY_SURFACE_TYPE_VIEW;
    surface->server = view->server;
    surface->view = view;
    surface->scene_tree = view->scene_tree;
    return surface;
}

flyaway_surface_t* new_flyaway_surface_from_popup(flyaway_popup_t* popup) {
    flyaway_surface_t* surface = calloc(1, sizeof(flyaway_surface_t));
    surface->type = FLYAWAY_SURFACE_TYPE_POPUP;
    surface->server = popup->server;
    surface->popup = popup;
    surface->scene_tree = popup->scene_tree;
    return surface;
}
