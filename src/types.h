#ifndef FLYAWAY_TYPES_H
#define FLYAWAY_TYPES_H

#include <assert.h>
#include <wayland-server-core.h>
#include <wlr/util/box.h>

#define WLR_USE_UNSTABLE
#include <wlr/backend.h>
#include <wlr/types/wlr_scene.h>
#include <wlr/types/wlr_xdg_shell.h>

typedef struct flyaway_server flyaway_server_t;
typedef struct flyaway_output flyaway_output_t;

typedef struct flyaway_xwayland flyaway_xwayland_t;

typedef struct flyaway_surface flyaway_surface_t;
typedef struct flyaway_popup flyaway_popup_t;
typedef struct flyaway_view flyaway_view_t;
typedef struct flyaway_xdg_view flyaway_xdg_view_t;
typedef struct flyaway_xwayland_view flyaway_xwayland_view_t;

typedef struct flyaway_keyboard flyaway_keyboard_t;

#endif
