#ifndef FLYAWAY_SURFACE_H
#define FLYAWAY_SURFACE_H

#include "types.h"

typedef enum { FLYAWAY_SURFACE_TYPE_VIEW, FLYAWAY_SURFACE_TYPE_POPUP } flyaway_surface_type_t;

struct flyaway_surface {
    flyaway_server_t* server;

    flyaway_surface_type_t type;
    struct wlr_scene_tree* scene_tree;

    union {
        flyaway_view_t* view;
        flyaway_popup_t* popup;
    };
};

flyaway_surface_t* new_flyaway_surface_from_view(flyaway_view_t* view);
flyaway_surface_t* new_flyaway_surface_from_popup(flyaway_popup_t* popup);

#endif
