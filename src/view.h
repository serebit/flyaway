#ifndef FLYAWAY_VIEW_H
#define FLYAWAY_VIEW_H

#include "types.h"
#include <wlr/xwayland.h>

typedef enum { FLYAWAY_VIEW_TYPE_XDG, FLYAWAY_VIEW_TYPE_XWAYLAND } flyaway_view_type_t;

struct flyaway_xdg_view {
    flyaway_view_t* base;

    struct wlr_xdg_toplevel* xdg_toplevel;
    struct wl_listener map;
    struct wl_listener unmap;
    struct wl_listener destroy;
    struct wl_listener commit;
    struct wl_listener request_move;
    struct wl_listener request_resize;
    struct wl_listener request_maximize;
    struct wl_listener request_unmaximize;
    struct wl_listener request_fullscreen;
};

struct flyaway_xwayland_view {
    flyaway_view_t* base;

    struct wlr_xwayland_surface* xwayland_surface;
    struct wl_listener map;
    struct wl_listener unmap;
    struct wl_listener destroy;
    struct wl_listener commit;
    struct wl_listener request_configure;
    struct wl_listener request_move;
    struct wl_listener request_resize;
    struct wl_listener set_geometry;
};

struct flyaway_view {
    flyaway_server_t* server;

    struct wl_list link;

    flyaway_view_type_t type;
    struct wlr_box current;
    struct wlr_box pending;
    struct wlr_box previous;
    struct wlr_surface* surface;
    struct wlr_scene_tree* scene_tree;
    struct wlr_scene_node* scene_node;

    union {
        flyaway_xdg_view_t* xdg_view;
        flyaway_xwayland_view_t* xwayland_view;
    };
};

flyaway_view_t* new_flyaway_xdg_view(flyaway_server_t* server, struct wlr_xdg_toplevel* toplevel);
flyaway_view_t* new_flyaway_xwayland_view(flyaway_server_t* server, struct wlr_xwayland_surface* surface);

#endif
