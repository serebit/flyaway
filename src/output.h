#ifndef FLYAWAY_OUTPUT_H
#define FLYAWAY_OUTPUT_H

#include "server.h"
#include "types.h"

struct flyaway_output {
    flyaway_server_t* server;

    struct wlr_box full_area;
    struct wlr_box usable_area;

    struct wl_list link;
    struct wlr_output* wlr_output;
    struct wl_listener frame;
    struct wl_listener destroy;
};

flyaway_output_t* new_flyaway_output(flyaway_server_t* server, struct wlr_output* wlr_output);

void flyaway_output_update_areas(flyaway_output_t* output);

#endif
