# flyaway

![Pipeline Status](https://gitlab.com/serebit/flyaway/badges/main/pipeline.svg)

Flyaway is a test compositor based on wlroots, intended as a playground for getting used to both wlroots and Wayland.

## Protocols

The following protocols are targets for implementation in Flyaway. If a protocol is checked off, it has been implemented in a way that I find satisfactory.

### Stable

- [ ] Presentation time
- [x] Viewporter (324b6a6)
- [ ] XDG shell

### Staging

- [x] XDG activation (2f75d23)
- [ ] DRM lease
- [ ] Session lock
- [x] Single-pixel buffer (324b6a6)
- [ ] Content type hint
- [x] Idle notify (b7ed380)
- [ ] Tearing control
- [ ] Xwayland shell
- [ ] Fractional scale

### Unstable

- [ ] Idle inhibit
- [ ] Pointer constraints
- [ ] XDG decoration
- [x] XDG output (f2b188e)

### WLR

- [ ] wlr layer shell

## License

Flyaway is licensed under the `Apache-2.0` open-source license.

Flyaway is based on tinywl, which is a minimal wlroots-based compositor located in the [wlroots](https://gitlab.freedesktop.org/wlroots/wlroots/)
source tree. wlroots is licensed under the `MIT` license, and tinywl inherits this license.
